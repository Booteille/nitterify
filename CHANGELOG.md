# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2019-09-16

### Added
- Add a button on the toolbar giving the ability to deactivate the extension
- Add backward compatibility with old versions
- Now handle redirection of medias from twimg.com

### Changed
- Change the structure of config file

## [0.3.0] - 2019-09-13

### Changed

- Now use Typescript, WebPack and Lodash

### Added

- Add an option page
- Add ability to select the desired Nitter instance 

## [0.2.0] - 2019-09-12

### Changed

- Simplify the extension

## [0.1.0] - 2019-09-12

### Added

- Initial release

[0.4.0]: https://gitlab.com/booteille/nitterify/compare/fd838523...a9a0ee88
[0.3.0]: https://gitlab.com/booteille/nitterify/compare/c72a71bc...fd838523
[0.2.0]: https://gitlab.com/booteille/nitterify/compare/a96b183b...c72a71bc
[0.1.1]: https://gitlab.com/booteille/nitterify/compare/d841dcd4...a96b183b
