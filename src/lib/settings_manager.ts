import { browser } from 'webextension-polyfill-ts';
import config from './config';
import * as _ from 'lodash';

/**
 * Load options
 *
 * @return Object
 */
export const load = async () => {
    return _.merge(config, await browser.storage.sync.get());
};

/**
 * Save settings to sync storage.
 *
 * @param settings
 * @return Promise<T>
 */
export const save = async (settings: object) => {
    await browser.storage.sync.set(settings);
};